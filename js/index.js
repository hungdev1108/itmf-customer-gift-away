// wow
new WOW().init();

// handle submit form
var form = document.getElementById("sheetdb-form");
form.addEventListener("submit", (e) => {
  e.preventDefault();
  fetch(form.action, {
    method: "POST",
    body: new FormData(document.getElementById("sheetdb-form")),
  })
    .then((response) => response.json())
    .then((html) => {
      // create alert element
      var alert = document.createElement("div");
      alert.classList.add("alert", "alert-success", "mt-3");
      alert.textContent = "ITMF cảm ơn bạn đã đăng ký 💙";

      // append alert element to form
      form.appendChild(alert);

      // clear form fields
      form.reset();

      setTimeout(function () {
        alert.remove();
      }, 5000);
    });
});

//handle submit
document.addEventListener("DOMContentLoaded", function () {
  const submitButton = document.querySelector(".btn-submit");
  const form = document.querySelector("#sheetdb-form");

  form.addEventListener("submit", function (event) {
    if (!submitButton.disabled) {
      submitButton.disabled = true;
      submitButton.textContent = "Đang xử lý...";

      setTimeout(function () {
        submitButton.disabled = false;
        submitButton.textContent = "Hoàn tất";
      }, 4000);
    } else {
      event.preventDefault();
    }
  });
});